-- Repeatable migrations have a description and a checksum,
-- but no version. Instead of being run just once,
-- they are (re-)applied every time their checksum changes.


-- This is very useful for managing database objects whose definition can then simply be maintained in a single file in version control.
-- They are typically used for:

-- (Re-)creating views/procedures/functions/packages/…
-- Bulk reference data reinserts
-- Within a single migration run, repeatable migrations are always applied last,
-- after all pending versioned migrations have been executed. Repeatable migrations are applied in the order of their description.

CREATE OR ALTER VIEW pokemons_with_a AS
    SELECT  Name FROM Pokemons WHERE Name LIKE 'a%';
GO