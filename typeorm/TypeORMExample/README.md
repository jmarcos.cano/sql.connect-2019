# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `npm i` command
2. Run `npm run db` to start a simple DataBase
3. Setup database settings inside `ormconfig.json` file
4. Run `npm start` command


# ToDO

- Dockerize app package.