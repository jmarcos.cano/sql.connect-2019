import "reflect-metadata";
import {createConnection} from "typeorm";
import {User} from "./entity/User";
import {Photo} from "./entity/Photo";




createConnection().then(async connection => {

    console.log("Inserting a new user into the database...");
    const user = new User();
    user.firstName = "Timber";
    user.lastName = "Saw";
    user.age = 25;
    await connection.manager.save(user);
    console.log("Saved a new user with id: " + user.id);

    console.log("Loading users from the database...");
    const users = await connection.manager.find(User);
    console.log("Loaded users: ", users);

    console.log("Here you can setup and run express/koa/any other framework.");

    console.log('-'.repeat(process.stdout.columns))


    console.log("Inserting photos now...")
    let photo =  new Photo();
    photo.name = "Me in Rome";
    photo.description = "My Vacation's photo";
    photo.filename = "rome.png";
    photo.views = 2;
    photo.isPublished = true;
    await connection.manager.save(photo)
    console.log("saving photo into DB...")
    let savedPhotos = await connection.manager.find(Photo)
    console.log("Photos: ", savedPhotos)

}).catch(error => console.log(error));
