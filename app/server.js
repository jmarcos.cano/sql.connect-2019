const sql = require('mssql')
var express = require('express');
var exphbs  = require('express-handlebars');
var helpers = require('handlebars-helpers')();
const fs = require('fs')
var favicon = require('serve-favicon')



try {
  var env_file = './.env'
  var etc_env_file = '/etc/config/env'

  if (fs.existsSync(env_file)) {
    require('dotenv').config({ path: env_file })
  }else if(fs.existsSync(etc_env_file)){
    require('dotenv').config({ path: etc_env_file })
  }
} catch(err) {
  console.error(err)
}

var app = express();


app.use(favicon(('./public/favicon.ico')))
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');
app.use('/static', express.static('public'))


console.log("Starting")


const config = {
    user: process.env.DB_USER || 'sa',
    password: process.env.DB_PASSWORD || 'P@ssw0rd',
    server: process.env.DB_HOST || 'localhost', // You can use 'localhost\\instance' to connect to named instance
    database: process.env.DATABASE || 'master',
}

console.dir(config)

app.get('/', function (req, res) {

    sql.connect(config, function (err) {

        if (err) console.log(err);

        // create Request object
        var request = new sql.Request();

        // query to the database and get the records
        request.query('select *  FROM [master].[dbo].[Pokemons]', function (err, recordset) {

            if (err) console.log(err)

            if ( typeof recordset == 'undefined' ){
                var recordset={}
            }
            var data="Hello"
            res.render('home', {data:recordset});
            console.log(recordset)
            sql.close()

        });
    });
});


app.get('/api', function (req, res) {

    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query('select *  FROM [master].[dbo].[Pokemons]', function (err, recordset) {
            if (err) console.log(err)

            if ( typeof recordset == 'undefined' ){
                var recordset={}
            }
            console.log(recordset)
            res.send(recordset);
            sql.close()
        });
    });
});

app.get('/status', function (req, res) {

    sql.connect(config, function (err) {
        if (err) console.log(err);
        var request = new sql.Request();
        request.query('SELECT TOP (1000) [installed_rank] ,[version] ,[description] ,[type] ,[script] ,[success] FROM [master].[myschema].[flyway_schema_history]', function (err, recordset) {
            if (err) {
                console.log(err)
            }
            try{
                var arr = recordset["recordset"]
                var latest = arr[arr.length-1]["version"];
            }catch(err) {
                console.error(err);
                var latest="None"
            }

            res.render('status', {data:recordset, latest: latest });
            console.log(recordset)
            sql.close()

        });
    });
});

var server = app.listen(5000, function () {
    console.log('Server is running..');
});