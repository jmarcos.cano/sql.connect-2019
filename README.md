[![pipeline](https://gitlab.com/jmarcos.cano/sql.connect-2019/badges/master/pipeline.svg)](https://gitlab.com/jmarcos.cano/sql.connect-2019/pipelines)

# CI/CD para bases de datos
### [SQL.Connect() - 2019](https://github.com/GTSSUG/SQLConnect/blob/master/Agenda.md)

> para ir al [Demo](docs/demo.md)

Este es el repositorio que usare para demo de "CI/CD para bases de datos", algunos de los temas que podras encontrar en este repositorio son:

- Kubernetes
- Docker
- Flyway
- Gitlab CI/CD
- YAML

# Descripcion
A continuacion cada uno de los folders

### - `app/`
Contiene el source code de la aplicacion Nodejs (Poke App), una aplicacion sencilla que:

- SELECT FROM Pokemons (de ser posible) y los muestra en una lista
- SELECT FROM flyway_schema_history (de ser posible) y despliega el resultado en una <tabla>

Algunos folders importantes:

- app/k8: contiene el kustomization para esta app, que esta compuesta de un deployment + Pod Template y un Service LoadBalancer.

### - `flyway`

Nuestras migraciones! 🎉, folder que contiene nuestras migraciones escritas en SQL dentro de flyway/sql

Dockerfile agrega estas migraciones (que esten dentro de ./sql en formato de Flyway) a la imagen recien generada y relacionada con nuestro PIPELINE.

Folders importantes:
- flyway/k8: hace referencia ../kustomization, en este caso en [Job Batch](https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/)


### - [`kustomization`](https://kustomize.io/)

Folder con los kustomize bases/resources para nuestro App y para Flyway migrations, usando Deploy + Service + Loadbalancer y Job/Batch respectivamente.

### - `mssql-in-gke`
Archivos necesarios para hacer deploy de SQL Server in Linux en cualquier cluster de Kubernetes, en este caso usamos GKE.

> Ojo que es un deployment bastante sencillo, sin HA.

### - [`typeorm`](https://typeorm.io/#/)

Otra aproximacion para migraciones de bases datos, usando TypeScript y ORM.

# [FlyWay](https://flywaydb.org/documentation/migrations)

Leer [Flyway Docs](https://flywaydb.org/documentation/migrations) para mas detalles acerca de Flyway



# [Demo](docs/demo.md)