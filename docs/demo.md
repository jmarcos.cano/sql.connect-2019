[🏠](../README.md)

# Demo 🍿

Prepare una serie de videos en donde muestro la demo un poco mas detenida y por si las cosas no salen bien 😉.

# Parte 0 - Intro

Explicacion de la estructura de folders


<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/CywuubBfl6o" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

# Parte 1

Explicación de Pipeline, Poke app y DB vacía.


<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/AMKb2_ECj14" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


# Parte 3

Full Demo de migraciones
- Utilizando migraciones con Flyway en Docker
- Gitlab para CI/CD
- MSSQL in kubernetes

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/ebpG4neEQdo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


[🏠](../README.md)
