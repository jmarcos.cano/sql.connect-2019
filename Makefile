.PHONY: all $(MAKECMDGOALS)

parent_dir:=$(shell echo $(CURDIR))
DATE=$(shell date)


COM_COLOR   := "\033[0;34m"
OBJ_COLOR   := "\033[0;36m"
OK_COLOR    := "\033[0;32m"
ERROR_COLOR := "\033[0;31m"
WARN_COLOR  := "\033[0;35m"
NO_COLOR    := "\e[0m"


PPRINT := @bash -c 'printf $(WARN_COLOR); printf "\n$${@} \n"; printf $(NO_COLOR);printf " \n\n"' MESSAGE
ERROR := @bash -c 'printf $(ERROR_COLOR); printf "\n [ERROR]: $${@} \n"; printf $(NO_COLOR)' MESSAGE
WARNING := @bash -c 'printf $(WARN_COLOR); printf "\n[WARNING]: $${@} \n"; printf $(NO_COLOR)' MESSAGE

local:
	cd flyway; make db ; cd -
	cd app; make up ; cd -

local_rm:
	cd flyway; make db ; cd -
	cd flyway; make rm ; cd -

build:
	echo "BLAHBLAH"

fly_rm:
	@cd flyway;make rm

fly_k8_rm:
	@cd flyway/k8/prod;make rm

migrate:
	cd flyway
	make up


# * ------------- MSSQL --------------- *
mssql:
	@cd mssql-in-gke;make

mssql_rm:
	@cd mssql-in-gke;make rm
mssql_restart:
	@cd mssql-in-gke;make restart


# * --------------- APP ---------------- *
app:
	cd app/k8;make

app_rm:
	cd app/k8;make clean

app_restart:
	cd app/k8;make restart

stack: app_restart mssql_restart


# * MIGRATIONS against K8 directly*
k8_migrate_V1:
	@cd flyway/k8/prod; make DOCKER_IMAGE=registry.gitlab.com/jmarcos.cano/sql.connect-2019/flyway/manual:V1 restart

k8_migrate_V2:
	@cd flyway/k8/prod; make DOCKER_IMAGE=registry.gitlab.com/jmarcos.cano/sql.connect-2019/flyway/manual:V2 restart

k8_migrate_V3:
	@cd flyway/k8/prod; make  DOCKER_IMAGE=registry.gitlab.com/jmarcos.cano/sql.connect-2019/flyway/manual:V3 restart

k8_migrate_V4:
	@cd flyway/k8/prod; make DOCKER_IMAGE=registry.gitlab.com/jmarcos.cano/sql.connect-2019/flyway/manual:V4 restart


#? MIGRATIONS USING GIT CI/CD
git_V1: fly_rm
	@ ${PPRINT} "Forcing V1 migrations"
	@echo $(DATE) >> flyway/sql/.dummy.txt
	@git add flyway/sql/ --force
	@git commit -m "V1 force [$(DATE)]"
	@git push origin master

git_V2:
	@ ${PPRINT} "Forcing ${@} migrations"
	@cp flyway/.migrations/V2* flyway/sql
	@echo "${@} $(DATE)" >> flyway/sql/.dummy.txt
	@git add flyway/sql/ --force
	@git commit -m "${@} force [$(DATE)]"
	@git push origin master

git_V3:
	@ ${PPRINT} "Forcing ${@} migrations"
	@cp flyway/.migrations/V2* flyway/sql
	@cp flyway/.migrations/V3* flyway/sql
	@echo "${@} $(DATE)" >> flyway/sql/.dummy.txt
	@git add flyway/sql/ --force
	@git commit -m "${@} force [$(DATE)]"
	@git push origin master

git_V4:
	@ ${PPRINT} "Forcing ${@} migrations"
	@cp flyway/.migrations/V2* flyway/sql
	@cp flyway/.migrations/V3* flyway/sql
	@cp flyway/.migrations/V4* flyway/sql
	@echo "${@} $(DATE)" >> flyway/sql/.dummy.txt
	@git add flyway/sql/ --force
	@git commit -m "${@} force [$(DATE)]"
	@git push origin master



# !WHOLE
git_reboot:
	@git add flyway/sql/ --force
	@git commit -m "${@} force [$(DATE)]"
	@git push origin master


reboot:  mssql_restart fly_rm fly_k8_rm git_reboot
